//
//  ViewController.swift
//  IMC
//
//  Created by Paulo Gutemberg on 17/04/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	
	@IBOutlet weak var tfPeso: UITextField!
	@IBOutlet weak var tfAltura: UITextField!
	@IBOutlet weak var lbResultado: UILabel!
	@IBOutlet weak var imResultado: UIImageView!
	
	@IBOutlet weak var viewResultado: UIView!
	var imc: Double = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}

	@IBAction func calcular(_ sender: Any) {
		if let peso = Double(self.tfPeso.text!), let altura = Double(self.tfAltura.text!) {
			imc = peso / pow(altura, 2) // altura elevada ao quadrado
			showResults()
		}
	}
	
	func showResults(){
		var resultado: String = ""
		var imagem : String = ""
		
		switch imc {
		case 0..<16:
			resultado = "Magreza"
			imagem = "abaixo"
		case 16..<18.5:
			resultado = "Abaixo do peso"
			imagem = "abaixo"
		case 18.5..<25:
			resultado = "Peso ideal"
			imagem = "ideal"
		case 25..<30:
			resultado = "Sobre peso"
			imagem = "sobre"
		default:
			resultado = "Obesidade"
			imagem = "obesidade"
		}
		
		lbResultado.text = " \(imc): \(resultado)"
		imResultado.image = UIImage(named: imagem)
		viewResultado.isHidden = false
		
		self.view.endEditing(true) // se nao tiver nem um text field sendo editado o teclado desaparece
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		// implementado sempre que tocam na tela
		self.view.endEditing(true)
	}
	//add
}

